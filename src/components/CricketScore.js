import React,{useEffect, useState} from 'react'
import '../styles/CricketScore.css';
function CricketScore() {
    const [score,setScore]=useState(0);
    const [wicket,setWicket]=useState(0);
    const [over,setOver]=useState(0);
    const [ball,setBall]=useState(0);

    useEffect(()=>{
     if(ball===6){
        setOver(over+1);
        setBall(0);
     }
     if(wicket>10){
        alert("Match Completed!");
        setWicket(10);
     }
    },[ball,wicket,over])
    const addScore=(num)=>{
        if(num!=='wicket' && wicket<10 && over<20){
            setScore(score+num);
            setBall(ball+1);
        }else if(num==='wicket' && wicket<10 && over<20){
            setWicket(wicket + 1);
            setBall(ball+1);
        }else{
            alert("Match Completed!");
            setWicket(10);
        }
        
    }
  return (
    <div className='container'>
        <h1>Cricket Score Board</h1>
        <div className="score">
        <h2>Score:<span>{score}/{wicket}</span></h2>
        <h2>Over:<span>{over}<span>.{ball}</span></span></h2>
        </div>
      <div className="btn">
      <button className='ball' onClick={()=>addScore(0)}>0</button>
         <button className='ball' onClick={()=>addScore(1)}>1</button>
         <button className='ball' onClick={()=>addScore(2)}>2</button>
         <button className='ball' onClick={()=>addScore(3)}>3</button>
         <button className='ball' onClick={()=>addScore(4)}>4</button>
         <button className='ball' onClick={()=>addScore(5)}>5</button>
         <button className='ball' onClick={()=>addScore(6)}>6</button>
         <button className='wicket' onClick={()=>addScore('wicket')}>Wicket</button>
      </div>
    </div>
  )
}

export default CricketScore